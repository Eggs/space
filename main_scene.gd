extends Node

var score = 0
var health = 1

# Laser variables
# Preload the screen we're going to use for our laser
var green_beam = preload("res://a_green_beam.scn")
var laser_count = 0
var laser_array = []
var laser_speed = 300

# Rock variables
var grey_rock = preload("res://grey_rock.scn")
var rock_count = 0
var rock_array = []
var rock_speed = -300
var time_since_last_rock = 0

# Ship variables
var ship_speed = 200
var ship_height = 90
var ship_width = 35

# Input variables
var is_space_pressed = false

# Screen variables
var screen_size

func _ready():
	screen_size = get_viewport_rect().size # MAKE SURE WORLD NODE HAS NOT MOVED!!
	get_node("Score").set_text(str(score))
	get_node("bgaudio").play("colour")
	isTop = true
	set_process(true) # This then calls _process

var game_running = true
# This function runs every frame
func _process(delta):
	if game_running == true:
		game_main(delta)
	else:
		if Input.is_action_pressed("Space"):
			restart_game()

func game_main(delta):
	# Make the game harder as the score goes up
	if score == 15:
		rock_speed = -400
		green_beam = preload("res://a_cyan_beam.scn")

	# Get the current position of the player and store it
	var ship_position = get_node("Player").get_pos()
	if health == 0:
		print("Game Over")

	# Make sure we can only fire once per keypress
	if Input.is_action_pressed("Space"):
		if is_space_pressed == false:
			ship_fire()
			is_space_pressed = true
	else:
		is_space_pressed = false
	
	# Detect if the left or right arrows are pressed and then set the ship_position variable. * by delta to keep const speed
	if Input.is_action_pressed("ui_left"):
		# Check if the ship is touching the left edge of the screen.
		# If it is then shop it moving
		if ship_position.x < screen_size.x / screen_size.x + ship_width:
			ship_position.x = ship_position.x
		else:
			ship_position.x = ship_position.x - ship_speed * delta
		
	if Input.is_action_pressed("ui_right"):
		# Check if the ship is touching the left edge of the screen.
		# If it is then shop it moving
		if ship_position.x >= screen_size.x - ship_width:
			ship_position.x = ship_position.x
		else:
			ship_position.x = ship_position.x + ship_speed * delta

	# In order for the ship to move we have to update the position
	get_node("Player").set_pos(ship_position)
	
	# Move the lasers
	var laserid = 0
	for laser in laser_array:
		var laser_position = get_node(laser).get_pos()
		laser_position.y = laser_position.y - laser_speed * delta
		get_node(laser).set_pos(laser_position)
		
		# If the laser has gone off the screen then delete it
		if laser_position.y < 0:
			remove_child(get_node(laser))
			laser_array.remove(laserid)
		laserid = laserid + 1

	# Move the rocks and check if laser has hit
	var rockid = 0
	for rock in rock_array:
		var rock_position = get_node(rock).get_pos()
		var ship_position = get_node("Player").get_pos()
		rock_position.y = rock_position.y - rock_speed * delta
		get_node(rock).set_pos(rock_position)
		if rock_position.y > screen_size.y:
			print("Deleted :" + str(rock))
			remove_child(get_node(rock))
			rock_array.remove(rockid)
		if rock_position.y >= ship_position.y - ship_width:
			if (rock_position.x - ship_width) < (ship_position.x + ship_width):
				if (rock_position.x + ship_width) > (ship_position.x - ship_width):
					health = 0
					get_node("Score").set_text("GAME OVER! \nPress FIRE to restart\n\n You scored: " + str(score))
					get_node("bgaudio").stop_all()
					game_running = false
			
		var laserid = 0
		for laser in laser_array:
			var laser_position = get_node(laser).get_pos()
			if rock_position.y > laser_position.y:
				if (rock_position.x - 20) < (laser_position.x):
					if (rock_position.x + 20) > (laser_position.x):
						get_node("audio").play("boom")
						remove_child(get_node(rock))
						remove_child(get_node(laser))
						rock_array.remove(rockid)
						laser_array.remove(laserid)
						score = score + 1
						get_node("Score").set_text(str(score))
			laserid = laserid + 1
		rockid = rockid + 1

	# Rock generator
	time_since_last_rock += delta
	if time_since_last_rock > rand_range(0.2, 1):
		new_rock()
		time_since_last_rock = 0

func ship_fire():
	laser_count = laser_count + 1 # Counter for naming the lasers
	var laser_instance = green_beam.instance() # Create an instance of the file
	laser_instance.set_name("laser" + str(laser_count)) # Name each instance a different name
	get_node("audio").play("pew")
	add_child(laser_instance) # Add it to the scene
	
	var laser_position = get_node("laser" + str(laser_count)).get_pos()
	var ship_position = get_node("Player").get_pos()
	
	laser_position.y = ship_position.y - ship_height
	laser_position.x = ship_position.x
	
	get_node("laser" + str(laser_count)).set_pos(laser_position)
	
	# Add the new laser to the laser_array
	laser_array.push_back("laser" + str(laser_count))
	
func new_rock():
	rock_count += 1
	var rock_instance = grey_rock.instance()
	rock_instance.set_name("rock" + str(rock_count))
	add_child(rock_instance)
	
	var rock_position = get_node("rock" + str(rock_count)).get_pos()
	var ship_position = get_node("Player").get_pos()
	
	rock_position.y = -12
	rock_position.x = rand_range(0, screen_size.x)
		
	get_node("rock" + str(rock_count)).set_pos(rock_position)
	
	rock_array.push_back("rock" + str(rock_count))
	
func restart_game():
	for rock in rock_array:
		remove_child(get_node(rock))
	rock_array.clear()
	
	for laser in laser_array:
		remove_child(get_node(laser))
	laser_array.clear()
	
	var ship_position = get_node("Player").get_pos()
	ship_position.x = screen_size.x /2
	get_node("Player").set_pos(ship_position)
	
	score = 0
	get_node("Score").set_text(str(score))
	health = 0
	get_node("bgaudio").play("colour")
	
	green_beam = preload("res://a_green_beam.scn")
	rock_speed = -300
	
	game_running = true
